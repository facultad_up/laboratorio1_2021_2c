package clase20210923;

public interface ModoCalculadora {
	String sumar(String numeroA, String numeroB);
	String restar(String numeroA, String numeroB);
}
