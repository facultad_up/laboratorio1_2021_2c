package clase20210923;

public class Calculadora {
	
	private ModoCalculadora modoOperacion;
	
	public Calculadora() {
		this.modoOperacion = new ModoDecimal();
	}
	
	public void cambiarModoOperacion(ModoCalculadora modoOperacion) {
		this.modoOperacion = modoOperacion;
	}

	public String sumar(String numeroA, String numeroB) {
		String res = this.modoOperacion.sumar(numeroA, numeroB);
		return res;
	}
	
	public String restar(String numeroA, String numeroB) {
		String res = this.modoOperacion.restar(numeroA, numeroB);
		return res;
	}
}
