package clase20210923;

public class Test {

	public static void main(String[] args) {
		Calculadora c = new Calculadora();
		String r = c.sumar("20", "40");
		
		System.out.println(r);
		
		c.cambiarModoOperacion(new ModoHexadecimal());
		r = c.sumar("A", "F");
		System.out.println(r);
	
		c.cambiarModoOperacion(new ModoBinario());
		r = c.sumar("1000", "10");
		System.out.println(r);
		
	}

}
