package correccion_parcial.ejercicio1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Agenda {
	private ArrayList<Contacto> contactos;
	private ArrayList<Cita> citas;

	public Agenda() {
		contactos = new ArrayList<Contacto>();
		citas = new ArrayList<Cita>();
	}

	public ArrayList<Contacto> getContactos() {
		return contactos;
	}

	public ArrayList<Cita> getCitas() {
		return citas;
	}

	public void listarContactosPorLocalidad(String localidad) {
		for (Contacto contacto : contactos) {
			Direccion direccion = contacto.getDireccion();
			if (direccion != null && direccion.getLocalidad().equalsIgnoreCase(localidad))
				System.out.println(contacto);
		}
	}

	public void listarCitasPorMesAnio(int mes, int anio) {
		for (Cita cita : citas) {
			if (cita.getMomento().getAnio() == anio && cita.getMomento().getMes() == mes) {
				System.out.println(cita);
			}
		}
	}

	public void listarCantidadDeCitasPorContacto() {
		int contador = 0;

		for (Contacto contacto : this.contactos) {
			for (Cita cita : this.citas) {
				if (cita.getContacto() == contacto)
					contador++;
			}
			System.out.println(contacto + ", cantidad citas: " + contador);
			contador = 0;
		}
	}

	public void listarContactosOrdenadosPorApellido() {
		Collections.sort(this.contactos);

		for (Contacto contacto : this.contactos) {
			System.out.println(contacto);
		}
	}
}
