package correccion_parcial.ejercicio1;

public class Principal {
	public static void main(String[] args) {
		Agenda agenda = new Agenda();
		Contacto contacto;
		Direccion direccion;
		Cita cita;

		direccion = new Direccion("San Mart�n", 111);
		direccion.setLocalidad("Santa Fe");
		direccion.setProvincia("Santa Fe");
		contacto = new Contacto("Juan", "Perez", direccion);
		contacto.getTelefonos().add(new Telefono(0342, 234523));

		agenda.getContactos().add(contacto);

		direccion = new Direccion("San Juan", 411);
		direccion.setLocalidad("Santa Cecila");
		direccion.setProvincia("Santa Fe");
		contacto = new Contacto("Alberto", "Perez", direccion);
		contacto.getTelefonos().add(new Telefono(0332, 23452));

		agenda.getContactos().add(contacto);

		direccion = new Direccion("Mendoza", 3211);
		direccion.setLocalidad("Santa Fe");
		direccion.setProvincia("Santa Fe");
		contacto = new Contacto("Juan", "Gomez", direccion);
		contacto.getTelefonos().add(new Telefono(0342, 234534));

		agenda.getContactos().add(contacto);

		cita = new Cita("Cafe", new Momento(1, 11, 2021, 20, 0));
		cita.setContacto(contacto);

		agenda.getCitas().add(cita);

		cita = new Cita("Examen", new Momento(3, 11, 2021, 10, 0));

		agenda.getCitas().add(cita);

		cita = new Cita("Trabajo", new Momento(20, 10, 2021, 19, 0));
		cita.setContacto(agenda.getContactos().get(0));

		agenda.getCitas().add(cita);

		// Punto b)
		System.out.println("\nContactos por Localidad:");
		agenda.listarContactosPorLocalidad("Santa Fe");

		// Punto c)
		System.out.println("\nCitas por Mes y a�o:");
		agenda.listarCitasPorMesAnio(11, 2021);

		// Punto d)
		System.out.println("\nContactos con cantidad de citas:");
		agenda.listarCantidadDeCitasPorContacto();

		// Punto e)
		System.out.println("\nContactos ordenados por apellido:");
		agenda.listarContactosOrdenadosPorApellido();
	}
}
