package correccion_parcial.ejercicio1;

public class Cita {
	private String motivo;
	private Momento momento;
	private Contacto contacto;

	public Cita(String motivo, Momento momento) {
		this.motivo = motivo;
		this.momento = momento;
	}

	public Cita(String motivo, Momento momento, Contacto contacto) {
		this(motivo, momento);
		this.contacto = contacto;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public Momento getMomento() {
		return momento;
	}

	public void setMomento(Momento momento) {
		this.momento = momento;
	}

	public Contacto getContacto() {
		return contacto;
	}

	public void setContacto(Contacto contacto) {
		this.contacto = contacto;
	}

	public String toString() {
		String res = "";
		res = "Cita " + motivo + ", momento=" + momento;
		if (this.contacto != null)
			res += ", con " + this.contacto;
				
		return res;
	}
}
