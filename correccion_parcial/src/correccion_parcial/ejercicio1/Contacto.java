package correccion_parcial.ejercicio1;

import java.util.ArrayList;

public class Contacto implements Comparable<Contacto> {
	private String nombre;
	private String apellido;
	private Direccion direccion;
	private ArrayList<Telefono> telefonos;

	public Contacto(String nombre, String apellido, Direccion direccion) {
		telefonos = new ArrayList<Telefono>();
		this.nombre = nombre;
		this.apellido = apellido;
		this.direccion = direccion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public ArrayList<Telefono> getTelefonos() {
		return telefonos;
	}

	public Direccion getDireccion() {
		return direccion;
	}

	public void setDireccion(Direccion direccion) {
		this.direccion = direccion;
	}
	
	public String toString() {
		return "Contacto [nombre=" + nombre + ", apellido=" + apellido + ", direccion=" + direccion + ", telefonos="
				+ telefonos + "]";
	}

	public int compareTo(Contacto o) {
		return this.getApellido().compareTo(o.getApellido());
	}
}
