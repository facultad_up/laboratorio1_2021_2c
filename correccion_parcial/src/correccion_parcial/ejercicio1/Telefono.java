package correccion_parcial.ejercicio1;

public class Telefono {
	private int area;
	private int telefono;

	public Telefono(int area, int telefono) {
		this.area = area;
		this.telefono = telefono;
	}

	public int getArea() {
		return area;
	}

	public void setArea(int area) {
		this.area = area;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public String toString() {
		return "(" + area + ") " + telefono;
	}
}
