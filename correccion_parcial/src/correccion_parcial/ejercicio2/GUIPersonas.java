package correccion_parcial.ejercicio2;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class GUIPersonas extends JFrame {

	private JTextField nombreBuscado, apellidoBuscado;
	private JButton buscar;
	private JTextField nombreResultado, apellidoResultado;

	private ListaPersonas lista;

	public GUIPersonas(ListaPersonas lp) {
		this.lista = lp;
		
		this.setSize(500, 300);
		this.setTitle("Ejercicio 2 Segundo Parcial");
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel superior = new JPanel();
		JPanel resultado = new JPanel();

		nombreBuscado = new JTextField(10);
		apellidoBuscado = new JTextField(10);
		buscar = new JButton("Buscar");

		superior.add(nombreBuscado);
		superior.add(apellidoBuscado);
		superior.add(buscar);
		this.add(superior, BorderLayout.NORTH);

		nombreResultado = new JTextField(10);
		apellidoResultado = new JTextField(10);

		resultado.add(nombreResultado);
		resultado.add(apellidoResultado);
		this.add(resultado);

		buscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Persona personaBuscada = lista.buscar(nombreBuscado.getText(), apellidoBuscado.getText());
				if (personaBuscada != null) {
					nombreResultado.setText(personaBuscada.getNombre());
					apellidoResultado.setText(personaBuscada.getApellido());
				} else {
					nombreResultado.setText("");
					apellidoResultado.setText("");
				}
			}
		});
	}
}
