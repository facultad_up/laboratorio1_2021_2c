package correccion_parcial.ejercicio2;

import javax.swing.SwingUtilities;

public class Principal {

	public static void main(String[] args) {
		ListaPersonas lp = new ListaPersonas();

		lp.agregarPersona(new Persona("Uno", "One"));
		lp.agregarPersona(new Persona("Dos", "Two"));
		lp.agregarPersona(new Persona("Tres", "Three"));
		lp.agregarPersona(new Persona("Cuatro", "Four"));
		lp.agregarPersona(new Persona("Cinco", "Five"));

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new GUIPersonas(lp);
			}
		});
	}
}
