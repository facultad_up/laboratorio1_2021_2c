package correccion_parcial.ejercicio2;

import java.util.ArrayList;

public class ListaPersonas {
	private ArrayList<Persona> lista;
	
	public ListaPersonas() {
		lista = new ArrayList<Persona>();
	}
	
	public void agregarPersona(Persona persona) {
		this.lista.add(persona);
	}
	
	public Persona buscar(String nombre, String apellido) {
		Persona persona = null;
		for (Persona per: lista) {
			if (per.getNombre().equalsIgnoreCase(nombre) && per.getApellido().equalsIgnoreCase(apellido))
				persona = per;
		}
		
		return persona;
	}
}
