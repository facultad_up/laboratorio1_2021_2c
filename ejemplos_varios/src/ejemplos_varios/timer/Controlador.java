package ejemplos_varios.timer;

public class Controlador implements IControlable {
	private Cocina cocina = new Cocina();
	private int cantidadCookies;
	
	public void realizarTarea() {
		int cookies = cocina.hacerCookies();
		
		this.cantidadCookies+=cookies;
		
		if (this.cantidadCookies % 5 == 0) {
			cocina.upgrate(1);
		}
		
		System.out.println("Actualizo todas las cosas: " + this.cantidadCookies);
	}

}
