package ejemplos_varios.timer;

public class Timer {
	private IControlable controlable;
	private long duracionEspera = 1000;
	
	public Timer(IControlable controlable) {
		this.controlable = controlable;
	}
	
	public void start() {
		long ultimaMedicion = 0;
		while (true) {
			if (System.currentTimeMillis()-ultimaMedicion > this.duracionEspera) {
				ultimaMedicion = System.currentTimeMillis();
				this.controlable.realizarTarea();
			}
		}
	}
	
	public static void main(String[] args) {
		Controlador contr = new Controlador();
		
		Timer timer = new Timer(contr);
		timer.start();

	}



}
