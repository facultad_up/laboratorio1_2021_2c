package ejemplos_varios.timer;

public class Cocina {
	private int cantidad = 1;
	
	public void upgrate(int incremento) {
		this.cantidad += incremento; 
	}
	
	public int hacerCookies() {
		System.out.println("Cocinando cookies");
		
		return cantidad;
	}
}
