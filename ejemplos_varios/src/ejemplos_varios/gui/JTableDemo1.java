package ejemplos_varios.gui;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class JTableDemo1 {
	private JFrame frame;

	public JTableDemo1() {
		frame = new JFrame("JTable Example");
		String[][] data = { 
			{ "Cell 1-1", "Cell 1-2", "Cell 1-3" },
			{ "Cell 2-1", "Cell 2-2", "Cell 3-3" }
		};
		String[] columnNames = { "Column One", "Column Two", "Column Three" };
		JTable table = new JTable(data, columnNames);

		frame.add(new JScrollPane(table));
		frame.setSize(400, 200);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new JTableDemo1();
			}
		});
	}
}
