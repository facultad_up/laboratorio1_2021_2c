package ejemplos_varios.gui;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

public class JTableModelDemo1 {

	private JFrame frame;

	class Person {
		private String lastname;
		private String fisrtname;
		private int age;

		public Person() {
		}
		
		public Person(String lastname, String fisrtname, int age) {
			this.lastname = lastname;
			this.fisrtname = fisrtname;
			this.age = age;
		}

		public String getLastname() {
			return lastname;
		}

		public void setLastname(String lastname) {
			this.lastname = lastname;
		}

		public String getFisrtname() {
			return fisrtname;
		}

		public void setFisrtname(String fisrtname) {
			this.fisrtname = fisrtname;
		}

		public int getAge() {
			return age;
		}

		public void setAge(int age) {
			this.age = age;
		}
	}

	class PersonTableModel extends AbstractTableModel {
		private String[] columnNames = {"Lastname", "Firstname", "Age"};
	    private List<Person> listPerson = new ArrayList<>();
	    
	    public PersonTableModel(List<Person> listPerson) {
	        this.listPerson.addAll(listPerson);
	    }
	    
		public int getRowCount() {
	        return this.listPerson.size();
	    }

		public int getColumnCount() {
			return this.columnNames.length;
		}
		
	    public String getColumnName(int column) {
	        return columnNames[column];
	    }

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			Object returnValue = null;
	        Person person = listPerson.get(rowIndex);
	         
	        switch (columnIndex) {
	        case 0:
	                returnValue = person.getLastname();
	            break;
	        case 1:
	                returnValue = person.getFisrtname();
	            break;
	        case 2:
	                returnValue = person.getAge();
	            break;
	        }
	         
	        return returnValue;
		}
	}

	public JTableModelDemo1() {
		frame = new JFrame("Custom TableModel Example");
		
		List<Person> listPerson = new ArrayList<>();
        listPerson.add(new Person("Last1", "One", 10));
        listPerson.add(new Person("Last2", "Two", 20));
        listPerson.add(new Person("Last3", "Three", 30));
        listPerson.add(new Person("Last4", "Four", 40));
		
		
        PersonTableModel tableModel = new PersonTableModel(listPerson);
		
		JTable table = new JTable(tableModel);

		frame.add(new JScrollPane(table));
		frame.setSize(400, 200);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new JTableModelDemo1();
			}
		});
	}
}
