package ejemplos_varios.gui;

import java.awt.Component;

import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

public class JTableEditDemo2 {
	
	private JFrame frame;
	
	class CellCheckBoxEditor extends DefaultCellEditor {
		public CellCheckBoxEditor() {
			super(new JCheckBox());
		}
	}
	
	class CellCheckBoxRender extends JCheckBox implements TableCellRenderer  {
		public Component getTableCellRendererComponent(JTable table, Object value, 
				boolean isSelected, boolean hasFocus, int row, int column) {
			
		    if (isSelected) {
				setForeground(table.getSelectionForeground());
				setBackground(table.getSelectionBackground());
			} else {
				setForeground(table.getForeground());
				setBackground(table.getBackground());
			}
			
			this.setSelected((value != null) && ((Boolean)value).booleanValue());
			return this;
		}
	}

	public JTableEditDemo2() {
		frame = new JFrame("JTable Example");
		
		Object[][] data = { 
				{ "Cell 1-1", "Cell 1-2", true }, 
				{ "Cell 2-1", "Cell 2-2", false } 
			};
		String[] columnNames = { "Column One", "Column Two", "Available" };
		JTable table = new JTable(data, columnNames);

		TableColumn col = table.getColumnModel().getColumn(2);
		col.setCellEditor(new CellCheckBoxEditor());
		col.setCellRenderer(new CellCheckBoxRender());
		
		frame.add(new JScrollPane(table));
		frame.setSize(400, 200);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new JTableEditDemo2();
			}
		});
	}
}
