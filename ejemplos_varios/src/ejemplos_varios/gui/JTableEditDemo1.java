package ejemplos_varios.gui;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.TableColumn;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class JTableEditDemo1 {
	private JFrame frame;
	
	class CellComboBoxEditor extends DefaultCellEditor {
		public CellComboBoxEditor(String[] items) {
			super(new JComboBox(items));
		}
	}

	public JTableEditDemo1() {
		frame = new JFrame("JTable Example");
		
		String[] regions = { "North", "South", "East", "West" };
		
		String[][] data = { 
				{ "Cell 1-1", "Cell 1-2", "North" }, 
				{ "Cell 2-1", "Cell 2-2", "South" } 
			};
		String[] columnNames = { "Column One", "Column Two", "Region" };
		JTable table = new JTable(data, columnNames);

		TableColumn col = table.getColumnModel().getColumn(2);
		col.setCellEditor(new CellComboBoxEditor(regions));
		
		frame.add(new JScrollPane(table));
		frame.setSize(400, 200);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new JTableEditDemo1();
			}
		});
	}
}
