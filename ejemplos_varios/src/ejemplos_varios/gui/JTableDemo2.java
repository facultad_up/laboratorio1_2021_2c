package ejemplos_varios.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.table.TableModel;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class JTableDemo2 {
	
	private JFrame frame;
	private JTextField rowNumber, colNumber;
	private JButton buttonGet;
	private JLabel cellValue;
	private TableModel model;
	
	public JTableDemo2() {
		frame = new JFrame("JTable Example");
		rowNumber = new JTextField(5);
		colNumber = new JTextField(5);
		cellValue = new JLabel();

		String[][] data = { 
				{ "Cell 1-1", "Cell 1-2", "Cell 1-3" }, 
				{ "Cell 2-1", "Cell 2-2", "Cell 3-3" } 
			};
		String[] columnNames = { "Column One", "Column Two", "Column Three" };
		JTable table = new JTable(data, columnNames);
		model = table.getModel();
		
		buttonGet = new JButton("Get Value");
		buttonGet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int row = Integer.parseInt(rowNumber.getText());
				int col = Integer.parseInt(colNumber.getText());
				cellValue.setText(model.getValueAt(row, col).toString());
			}
		});
		
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(1, 4));
		panel.add(rowNumber);
		panel.add(colNumber);
		panel.add(buttonGet);
		panel.add(cellValue);

		frame.add(new JScrollPane(table));
		frame.add(panel, BorderLayout.NORTH);
		frame.setSize(400, 200);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new JTableDemo2();
			}
		});
	}
}
