package strategy;

/*
 * Patron de dise�o: Strategy
 * 
 */

public class Test {

	public static void main(String[] args) {
		Persona p = new Persona("Pepe", "Pip", 20);
		p.realizarSaludo();

		p.setEstadoDeAnimo(new Feliz());
		p.realizarSaludo();
		
		p.setEstadoDeAnimo(new Alegre());
		p.realizarSaludo();
	}

}
