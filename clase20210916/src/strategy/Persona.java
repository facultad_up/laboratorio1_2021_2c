package strategy;

public class Persona {
	private String nombre, apellido;
	private int edad;

	private EstadoDeAnimo estado;
	
	public Persona(String nombre, String apellido, int edad) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.edad = edad;
		
		this.estado = new Enojado();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public void setEstadoDeAnimo(EstadoDeAnimo estado) {
		this.estado = estado;
	}
	
	public String toString() {
		return "Persona [nombre=" + nombre + ", apellido=" + apellido + ", edad=" + edad + "]";
	}
	
	public void realizarSaludo() {
		this.estado.saludar();
	}
	
}
