package clase20210916.singleton;

public class TestContador {

	public static void main(String[] args) {
//		ContadorUnico cu = new ContadorUnico();
//		ContadorUnico cu2 = new ContadorUnico();
		
		ContadorUnico cu = ContadorUnico.getInstance();
		ContadorUnico cu2 = ContadorUnico.getInstance();
		
		cu.incrementar();
		cu.incrementar();
		System.out.println(cu.getContador());

	}

}
