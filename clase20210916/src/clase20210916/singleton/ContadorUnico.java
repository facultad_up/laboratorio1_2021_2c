package clase20210916.singleton;

/*
 * Padron de dise�o: Singleton
 * 
 * 1- Poner el constructor en privado
 * 2- Crear un metodo estatico para acceder a la unica instancia
 * 3- Crear la variable estatica de la instancia
 * 4- Instanciar una unica vez en la clase
 */
public class ContadorUnico {
	private int contador;
	private static ContadorUnico contadorUnico;
	
	private ContadorUnico() {
		
	}
	
	public static ContadorUnico getInstance() {
		if (contadorUnico == null)
			contadorUnico = new ContadorUnico();
		
		return contadorUnico;
	}
	
	public int getContador() {
		return this.contador;
	}
	
	public void incrementar() {
		this.contador++;
	}
	
	public void decrementar() {
		this.contador--;
	}
	
}
