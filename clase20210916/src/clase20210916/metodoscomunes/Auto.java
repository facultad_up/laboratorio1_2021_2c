package clase20210916.metodoscomunes;

public class Auto {
	private String patente;
	private String color;
	
	public Auto(String patente, String color) {
		this.patente = patente;
		this.color = color;
	}

	public String getPatente() {
		return patente;
	}

	public void setPatente(String patente) {
		this.patente = patente;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String toString() {
		return "Auto [patente=" + patente + ", color=" + color + "]";
	}
	
	public boolean equals(Object o) {
		return this.patente == ((Auto)o).patente;
	}
}
