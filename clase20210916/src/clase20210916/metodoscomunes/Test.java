package clase20210916.metodoscomunes;

public class Test {

	public static void main(String[] args) {
		Auto a = new Auto("123", "Gris"); // Cargado por el usuario
		Auto b = new Auto("123", "Gris"); // Recuperado de la bd

		//if (a == b) {
		
		if (a.equals(b)) {
			System.out.println("Mismo auto.");
		} else {
			System.out.println("No lo son");
		}
		
	}

}
