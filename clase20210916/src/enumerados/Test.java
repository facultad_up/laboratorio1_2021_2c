package enumerados;

public class Test {

	public static void main(String[] args) {
		Colores c = Colores.AMARILLO;
		
		if (c == Colores.AMARILLO) {
			System.out.println("Yellow");			
			System.out.println(c.getRgb());
		}
		
		switch (c) {
		case AMARILLO:
			System.out.println("Yellow");
			break;
		
		case ROJO:
			System.out.println("Red");
			break;

		case AZUL:
			System.out.println("Blue");
			break;
		
		default:
			System.out.println("No se");
			break;
		}
		
	}

}

