package sobrecarga;

import java.util.Scanner;

public class Calculadora extends EquipoElectronico {
	private int resultado;
	private String leyenda = "Resultado:";
	
	public int getResultado() {
		return resultado;
	}
	
	public void sumar(int numero) {
		System.out.println("    Sumar v1");
		resultado += numero;
	}
	
	public void sumar(int numeroA, int numeroB) {
		System.out.println("    Sumar v2");
		resultado = numeroA + numeroB;
	}
	
	public void sumar(int numero, String leyenda) {
		System.out.println("    Sumar v3");
		this.sumar(numero);
		this.leyenda = leyenda;
	}
	
	public void sumar(String leyenda, int numero) {
		System.out.println("    Sumar v4");
		this.sumar(numero);
		this.leyenda = leyenda;
	}
	
	
	public String toString() {
		return leyenda + " " + resultado;
	}
	
	
	
	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
		
		Calculadora c = new Calculadora();
		
		System.out.print("Ingrese numero: ");
		int nro1 = teclado.nextInt();
		
		c.sumar(nro1);
		
		System.out.print("Ingrese numero: ");
		nro1 = teclado.nextInt();
		
		c.sumar(nro1);
		
		System.out.println(c);
		
		System.out.print("Ingrese numero: ");
		int nro2 = teclado.nextInt();
		
		System.out.print("Ingrese numero: ");
		int nro3 = teclado.nextInt();
		
		c.sumar(nro2, nro3);
		System.out.println(c);
		
		c.sumar(nro1, "Esto vale:");
		System.out.println(c);
		
		c.sumar("Esto vale:", nro2);
		System.out.println(c);
		
	}
}
