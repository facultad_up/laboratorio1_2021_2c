package herencia;

public class Test {

	public static void main(String[] args) {
//		Gato g = new Gato();
//		g.maullar();
//		g.comer();
//		g.saltar();
//		System.out.println(g);
		
		GatoSiames gs = new GatoSiames();
		gs.maullar();
		gs.molestar();
		gs.saludar();
		
//		Animal a = new Animal();
//		a.comer();
		System.out.println();
		
		Perro p = new Perro();
		p.saludar();
	}

}
