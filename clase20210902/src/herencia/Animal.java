package herencia;

public abstract class Animal {
	protected float peso = 300;
	
	
	public void comer() {
		System.out.println("Comiendo...");
	}
	
	public void saltar() {
		System.out.println("Saltando...");
	}
	
	public abstract void saludar();
	
}
