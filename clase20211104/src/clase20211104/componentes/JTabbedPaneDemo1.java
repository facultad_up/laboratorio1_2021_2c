package clase20211104.componentes;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;

public class JTabbedPaneDemo1 {

	public JTabbedPaneDemo1() {
		JFrame frame = new JFrame("JTabbedPane Example");

	    JTabbedPane tp=new JTabbedPane();
		tp.addTab("Tab 1", new JPanel());
		tp.addTab("Tab 2", new JPanel());
		tp.addTab("Tab 3", new JPanel());
		
		frame.add(tp);
		frame.setSize(400, 200);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new JTabbedPaneDemo1();
			}
		});
	}
}

