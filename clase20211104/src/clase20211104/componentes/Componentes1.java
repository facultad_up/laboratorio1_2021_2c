package clase20211104.componentes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class Componentes1 extends JFrame { // BorderLayout

	public Componentes1() {
		this.setSize(300, 200);
		this.setTitle("Panel");
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel1 = new JPanel(); // FlowLayout

		JButton btn1 = new JButton("Click me");
		panel1.add(btn1);

		JTextField texto = new JTextField(10);
		panel1.add(texto);

		this.add(panel1);

		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Me clickearon");
			}
		});

		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("otro clicker");
			}
		});

		texto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("tipeado");
			}
		});

		texto.addKeyListener(new KeyListener() {
			public void keyTyped(KeyEvent e) {
				System.out.println("Tipeando..." + e.getKeyChar());
			}

			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});

		texto.addMouseListener(new MouseListener() {
			public void mouseClicked(MouseEvent e) {
				System.out.println(e.getButton());
			}

			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}
		});
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new Componentes1();
			}
		});
	}

}
