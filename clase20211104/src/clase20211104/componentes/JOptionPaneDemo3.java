package clase20211104.componentes;

import javax.swing.JOptionPane;

public class JOptionPaneDemo3 {
	
	public static void main(String[] args) {
		 String input = JOptionPane.showInputDialog(null, "Input your name:", 
				 			"Input", JOptionPane.QUESTION_MESSAGE);
		 
		 System.out.println("Hello " + input);
	}
}


