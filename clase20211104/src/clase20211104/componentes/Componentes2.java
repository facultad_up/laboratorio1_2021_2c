package clase20211104.componentes;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class Componentes2 {
	private JFrame frame1;

	Componentes2() {
		frame1 = new JFrame();

		JButton btn1 = new JButton("+");
		JButton btn2 = new JButton("-");
		JTextField result = new JTextField(10);
		JTextField numberA = new JTextField(10);
		JTextField numberB = new JTextField(10);
		
		result.setEditable(false);
		
		JPanel panelOptions = new JPanel();
		panelOptions.add(btn1);
		panelOptions.add(btn2);

		JPanel panel1 = new JPanel();
		panel1.setLayout(new GridLayout(1, 2));
		panel1.add(numberA);
		panel1.add(numberB);
		
		JPanel panel2 = new JPanel();
		panel2.setLayout(new GridLayout(1, 2));
		panel2.add(new JLabel("Result:"));
		panel2.add(result);

		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int a = Integer.parseInt(numberA.getText());
				int b = Integer.parseInt(numberB.getText());
				int r = a + b;
				result.setText(Integer.toString(r));
			}
		});
		
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int a = Integer.parseInt(numberA.getText());
				int b = Integer.parseInt(numberB.getText());
				int r = a - b;
				result.setText(Integer.toString(r));
			}
		});
		
		frame1.add(panelOptions);
		frame1.add(panel1);
		frame1.add(panel2);

		frame1.setLayout(new GridLayout(3, 1));
		frame1.setSize(500, 200);
		frame1.setVisible(true);
		frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new Componentes2();
			}
		});
	}
}

