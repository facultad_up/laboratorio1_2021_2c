package clase20211104.componentes;

import javax.swing.JOptionPane;

public class JOptionPaneDemo2 {
	
	public static void main(String[] args) {
		 int input = JOptionPane.showConfirmDialog(null, 
	                		"Do you want to proceed?", 
	                		"Select an Option...", JOptionPane.YES_NO_CANCEL_OPTION);
		 
		 System.out.println(input);
	}
}
