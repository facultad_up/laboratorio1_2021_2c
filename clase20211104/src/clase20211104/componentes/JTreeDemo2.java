package clase20211104.componentes;

import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;

public class JTreeDemo2 {
	public JTreeDemo2() {
		JFrame frame = new JFrame("JTree Example");
		JLabel label = new JLabel("");

		DefaultMutableTreeNode root = new DefaultMutableTreeNode("Root");
		DefaultMutableTreeNode branch1 = new DefaultMutableTreeNode("Branch 1");
		DefaultMutableTreeNode branch2 = new DefaultMutableTreeNode("Branch 2");
		DefaultMutableTreeNode branch1A = new DefaultMutableTreeNode("Branch 1A");
		DefaultMutableTreeNode branch1B = new DefaultMutableTreeNode("Branch 1B");

		root.add(branch1);
		root.add(branch2);

		branch1.add(branch1A);
		branch1.add(branch1B);

		JTree tree = new JTree(root);
		frame.add(tree);
		frame.add(label);

		tree.addTreeSelectionListener(new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent e) {
				label.setText(e.getPath().getLastPathComponent().toString());
			}
		});
		
		frame.setLayout(new GridLayout(1, 2));
		frame.setSize(300, 200);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new JTreeDemo2();
			}
		});
	}
}
