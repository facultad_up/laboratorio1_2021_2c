package clase20211104.componentes;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

public class JMenuDemo1 {

	JMenu menu, submenu;
	JMenuItem i1, i2, i3, i4, i5;

	public JMenuDemo1() {
		JFrame frame = new JFrame("Menu and MenuItem Example");
		JMenuBar mb = new JMenuBar();
		
		menu = new JMenu("Menu");
		i1 = new JMenuItem("Item 1");
		i2 = new JMenuItem("Item 2");
		i3 = new JMenuItem("Item 3");
		
		menu.add(i1);
		menu.add(i2);
		menu.add(i3);
		
		submenu = new JMenu("Sub Menu");
		i4 = new JMenuItem("Item 4");
		i5 = new JMenuItem("Item 5");
		submenu.add(i4);
		submenu.add(i5);
		
		menu.add(submenu);
		
		mb.add(menu);
		frame.setJMenuBar(mb);
		frame.setSize(400, 400);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new JMenuDemo1();
			}
		});
	}
}

