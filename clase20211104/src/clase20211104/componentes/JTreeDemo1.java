package clase20211104.componentes;

import javax.swing.JFrame;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;

public class JTreeDemo1 {
	public JTreeDemo1() {
		JFrame frame = new JFrame("JTree Example");
		
		DefaultMutableTreeNode root = new DefaultMutableTreeNode("Root");
		DefaultMutableTreeNode branch1 = new DefaultMutableTreeNode("Branch 1");
		DefaultMutableTreeNode branch2 = new DefaultMutableTreeNode("Branch 2");
		DefaultMutableTreeNode branch1A = new DefaultMutableTreeNode("Branch 1A");
		DefaultMutableTreeNode branch1B = new DefaultMutableTreeNode("Branch 1B");
		
		root.add(branch1);
		root.add(branch2);
		
		branch1.add(branch1A);
		branch1.add(branch1B);
		
		JTree tree = new JTree(root);
		frame.add(tree);
		
		frame.setSize(300, 200);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new JTreeDemo1();
			}
		});
	}
}
