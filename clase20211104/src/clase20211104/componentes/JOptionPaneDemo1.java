package clase20211104.componentes;

import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class JOptionPaneDemo1 {
	private JFrame frame;
	
	public JOptionPaneDemo1() {
		frame = new JFrame();

		JOptionPane.showMessageDialog(frame, "This is a example text", 
				"JOptionPane Example", JOptionPane.ERROR_MESSAGE);
		
		frame.setLayout(new GridLayout(4, 1));
		frame.setSize(300, 200);
		frame.setVisible(true);
		frame.setTitle("JComboBox Demo");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new JOptionPaneDemo1();
			}
		});
	}
}
