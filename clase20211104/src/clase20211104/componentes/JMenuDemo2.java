package clase20211104.componentes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

public class JMenuDemo2 {

	JMenu menu, submenu;
	JMenuItem i1, i2, i3, i4, i5;
	JLabel label;
	
	public JMenuDemo2() {
		JFrame frame = new JFrame("Menu and MenuItem Example");
		JMenuBar mb = new JMenuBar();
		label = new JLabel("");

		menu = new JMenu("Menu");
		i1 = new JMenuItem("Item 1");
		i2 = new JMenuItem("Item 2");
		i3 = new JMenuItem("Item 3");
		
		menu.add(i1);
		menu.add(i2);
		menu.add(i3);
		
		i1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				label.setText("Clicked Item 1!");
			}
		});
		
		mb.add(menu);

		frame.setJMenuBar(mb);
		frame.add(label);
		frame.setSize(300, 200);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new JMenuDemo2();
			}
		});
	}
}

