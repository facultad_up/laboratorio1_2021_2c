package clase20211104.componentes;

import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.SwingUtilities;

public class JRadioButtonDemo {
	private JFrame frame;
	
	public JRadioButtonDemo() {
		frame = new JFrame();

		JLabel label = new JLabel("Drink coffee?");
		JRadioButton yesButton   = new JRadioButton("Yes"  , true);
		JRadioButton noButton    = new JRadioButton("No"   , false);
		JRadioButton maybeButton = new JRadioButton("Maybe", false);

		ButtonGroup bgroup = new ButtonGroup();
		bgroup.add(yesButton);
		bgroup.add(noButton);
		bgroup.add(maybeButton);
		
		frame.add(label);
		frame.add(yesButton);
		frame.add(noButton);
		frame.add(maybeButton);

		frame.setLayout(new GridLayout(4, 1));
		frame.setSize(300, 200);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new JRadioButtonDemo();
			}
		});
	}
}

