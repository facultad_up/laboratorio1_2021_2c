package clase20211104.componentes;

import java.awt.GridLayout;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

class Person {
	private String firstname;
	private String lastname;
	
	public Person(String firstname, String lastname) {
		this.firstname = firstname;
		this.lastname = lastname;
	}
	
	public String toString() {
		return this.firstname + " " + this.lastname;
	}
}

@SuppressWarnings({ "rawtypes", "unchecked" })
public class JComboBoxDemo2 {
	private JFrame frame;
	
	public JComboBoxDemo2() {
		frame = new JFrame();

		Person[] options = {
				new Person("FN1", "LN1"), 
				new Person("FN2", "LN2"), 
				new Person("FN3", "LN3")
		};
		
		JComboBox comboBox1 = new JComboBox(options);
		
		frame.add(new JLabel("Select a person:"));
		frame.add(comboBox1);
		
		frame.setLayout(new GridLayout(4, 1));
		frame.setSize(300, 200);
		frame.setVisible(true);
		frame.setTitle("JComboBox Demo");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new JComboBoxDemo2();
			}
		});
	}
}
