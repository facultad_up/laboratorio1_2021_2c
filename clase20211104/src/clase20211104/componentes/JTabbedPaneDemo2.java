package clase20211104.componentes;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;

public class JTabbedPaneDemo2 {

	public JTabbedPaneDemo2() {
		JFrame frame = new JFrame("JTabbedPane Example");
		JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel();
		JPanel panel3 = new JPanel();
		
		panel1.setLayout(new GridLayout(2, 2));
		panel1.add(new JButton("Button A"));
		panel1.add(new JButton("Button B"));
		panel1.add(new JButton("Button C"));
		panel1.add(new JButton("Button D"));
		

	    JTabbedPane tp=new JTabbedPane();
		tp.addTab("Tab 1", panel1);
		tp.addTab("Tab 2", panel2);
		tp.addTab("Tab 3", panel3);
		
		frame.add(tp);
		frame.setSize(400, 200);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new JTabbedPaneDemo2();
			}
		});
	}
}

