package clase20211104.componentes;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Componentes3 {
	private JFrame frame1;

	Componentes3() {
		frame1 = new JFrame();

		JButton btn1 = new JButton("Add");
		JButton btn2 = new JButton("Get");
		JButton btn3 = new JButton("Remove");
		
		JPanel panelOptions = new JPanel();
		panelOptions.add(btn1);
		panelOptions.add(btn2);
		panelOptions.add(btn3);
		
		JPanel panel1 = new JPanel();
		panel1.setLayout(new GridLayout(1, 2));
		panel1.add(new JLabel("Firstname:"));
		panel1.add(new JTextField(20));
		
		JPanel panel2 = new JPanel();
		panel2.setLayout(new GridLayout(1, 2));
		panel2.add(new JLabel("Lastname:"));
		panel2.add(new JTextField(20));

		frame1.add(panelOptions);
		frame1.add(panel1);
		frame1.add(panel2);

		frame1.setLayout(new GridLayout(3, 1));
		frame1.setSize(500, 200);
		frame1.setVisible(true);
	}

	public static void main(String[] args) {
		new Componentes3();
	}
}

