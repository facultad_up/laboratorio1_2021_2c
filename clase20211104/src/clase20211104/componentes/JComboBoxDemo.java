package clase20211104.componentes;

import java.awt.GridLayout;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class JComboBoxDemo {
	private JFrame frame;
	
	public JComboBoxDemo() {
		frame = new JFrame();

		String[] options = {"Red", "Blue", "Green", "Yellow"};
		
		JComboBox comboBox1 = new JComboBox(options);
		
		frame.add(new JLabel("Select one color:"));
		frame.add(comboBox1);
		
		frame.setLayout(new GridLayout(4, 1));
		frame.setSize(300, 200);
		frame.setVisible(true);
		frame.setTitle("JComboBox Demo");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new JComboBoxDemo();
			}
		});
	}
}
