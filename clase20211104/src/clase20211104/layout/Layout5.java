package clase20211104.layout;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class Layout5 extends JFrame {

	public Layout5() {
		this.setSize(300, 200);
		this.setTitle("Ventana 2");
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		
		JButton btn1 = new JButton("Centro");
		JButton btn2 = new JButton("North");
		JButton btn3 = new JButton("South");
		JButton btn4 = new JButton("East");
		JButton btn5 = new JButton("West");
		
		panel.add(btn1);
		panel.add(btn2);
		panel.add(btn3);
		panel.add(btn4);
		panel.add(btn5);
		
		this.add(panel);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new Layout5();
			}
		});
	}

}
