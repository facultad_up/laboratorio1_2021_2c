package clase20211104.layout;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class Layout1 extends JFrame {	// BorderLayout

	public Layout1() {
		this.setSize(300, 200);
		this.setTitle("Ventana 2");
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JButton btn1 = new JButton("Centro");
		JButton btn2 = new JButton("North");
		JButton btn3 = new JButton("South");
		JButton btn4 = new JButton("East");
		JButton btn5 = new JButton("West");
		
		this.add(btn1);
		this.add(btn2, BorderLayout.NORTH);
		this.add(btn3, BorderLayout.SOUTH);
		this.add(btn4, BorderLayout.EAST);
		this.add(btn5, BorderLayout.WEST);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new Layout1();
			}
		});
	}

}
