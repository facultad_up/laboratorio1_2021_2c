package clase20211104.layout;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class Layout2 extends JFrame {

	public Layout2() {
		this.setSize(300, 200);
		this.setTitle("Ventana 2");
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.setLayout(new GridLayout(3, 4));
		
		for (int i=0; i<14; i++) {
			JButton btn1 = new JButton("Boton: " + i);
			this.add(btn1);
		}
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new Layout2();
			}
		});
	}

}
