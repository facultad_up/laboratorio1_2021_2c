package clase20211104.layout;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class Layout4 extends JFrame {

	public Layout4() {
		this.setSize(300, 200);
		this.setTitle("Ventana 2");
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.setLayout(null);
		
		JButton btn1 = new JButton("Centro");
		JButton btn2 = new JButton("North");
		JButton btn3 = new JButton("South");
		JButton btn4 = new JButton("East");
		JButton btn5 = new JButton("West");
		
		this.add(btn1);
		btn1.setBounds(10, 10, 100, 30);
		
		this.add(btn2);
		btn2.setBounds(10, 50, 100, 30);
		
		this.add(btn3);
		this.add(btn4);
		this.add(btn5);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new Layout4();
			}
		});
	}

}
