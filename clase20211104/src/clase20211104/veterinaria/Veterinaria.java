package clase20211104.veterinaria;

import java.util.ArrayList;

public class Veterinaria {
	private ArrayList<Gato> gatos;
	
	public Veterinaria() {
		gatos = new ArrayList<Gato>();
	}
	
	public void agregarGato(Gato gato) {
		this.gatos.add(gato);
	}
	
	public void mimarGato(String nombre) {
		Gato gato = gatos.get(gatos.indexOf(new Gato(nombre)));
		gato.mimar();
	}
}
