package clase20211104.veterinaria;

import javax.swing.Icon;

public class Gato {
	private int mimos;
	private String nombre;

	public Gato(String nombre) {
		this.nombre = nombre;
	}
	
	public int getMimos() {
		return this.mimos;
	}
	
	public void mimar() {
		mimos++;
		System.out.println("brrbrrbrrr " + nombre);
	}

	public boolean equals(Object obj) {
		return ((Gato)obj).nombre.equals(this.nombre);
	}

	public String getNombre() {
		return this.nombre;
	}
}
