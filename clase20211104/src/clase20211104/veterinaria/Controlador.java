package clase20211104.veterinaria;

public class Controlador implements IMetodosPrincipal {
	private Veterinaria v;
	private VeterinariaGUI g; 
	
	public Controlador() {
		v = new Veterinaria();
		g = new VeterinariaGUI(this);
	}
	
	public void agregarGato(String nombre) {
		// Agrega el gato al modelo
		Gato gato = new Gato(nombre);
		v.agregarGato(gato);
		
		// Notifica a grafica
		g.gatoAgregado(gato);
	}

	public static void main(String[] args) {
		new Controlador();
	}


}
