package clase20211104.veterinaria;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class VeterinariaGUI extends JFrame {
	private JPanel panel;
	private IMetodosPrincipal principal;
	private JLabel contadorMimos;
	
	public VeterinariaGUI(IMetodosPrincipal principal) {
		this.principal = principal;
		
		this.setTitle("Veterinaria");
		this.setSize(400, 500);
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		panel = new JPanel();
		this.add(panel);
		JButton agregarGato = new JButton("Agregar Gato");
		this.add(agregarGato, BorderLayout.SOUTH);
		
		contadorMimos = new JLabel("Cantidad mimos realizados: 0");
		this.add(contadorMimos, BorderLayout.NORTH);
		
		agregarGato.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nombre = JOptionPane.showInputDialog("Ingrese nombre:");
				principal.agregarGato(nombre);
			}		
		});
	}
	
	public void gatoAgregado(Gato gato) {
		JButton btn = new JButton(gato.getNombre());
		panel.add(btn);
		panel.doLayout();
		btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				gato.mimar();
			}
		});
	}
	
	public void agregarGato(Gato gato) {
		JButton btn = new JButton(gato.getNombre());
		panel.add(btn);
		
	}
}
