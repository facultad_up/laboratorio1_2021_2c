package clase20211104.paneles;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class Paneles1 extends JFrame {	// BorderLayout

	public Paneles1() {
		this.setSize(300, 200);
		this.setTitle("Panel");
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel1 = new JPanel();	// FlowLayout
		
		JButton btn1 = new JButton("Centro");
		JButton btn2 = new JButton("North");
		JButton btn3 = new JButton("South");
		JButton btn4 = new JButton("East");
		JButton btn5 = new JButton("West");
		
		panel1.add(btn1);
		panel1.add(btn2, BorderLayout.NORTH);
		panel1.add(btn3, BorderLayout.SOUTH);
		panel1.add(btn4, BorderLayout.EAST);
		panel1.add(btn5, BorderLayout.WEST);
		
		this.add(new JButton("Soy el norte"), BorderLayout.NORTH);
		this.add(panel1);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new Paneles1();
			}
		});
	}

}
