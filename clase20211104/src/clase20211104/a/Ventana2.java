package clase20211104.a;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class Ventana2 extends JFrame {
	
	public Ventana2() {
		this.setSize(300, 200);
		this.setTitle("Ventana 2");
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new Ventana2();
			}
		});
	}
}
