package clase20211104.a;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class Ventana1 {
	private JFrame frame;
	
	public Ventana1() {
		frame = new JFrame();
		frame.setSize(300, 200);
		frame.setTitle("Ventana 1");
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new Ventana1();
			}
		});
	}

}
