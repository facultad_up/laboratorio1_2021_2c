package clase20210819;

public class Auto {		// Todas las clases heredan de Object
	// Atributos o variables de estado 
	// Todas las var. de estado: Se autoinicializan
	private String patente;		// variable de referencia a un obj del tipo String
	private int kilometrosRecorridos;
	private boolean encendido;
	private int cantidadCombustible = 50;
	
	// Metodo constructor
	public Auto(String unaPatente) {
		System.out.println("Creando un auto...");
		patente = unaPatente;
	}
	
	
	// Getters y Setters
	public String getPatente() {
		return patente;
	}
	
	public int getKilometrosRecorridos() {
		return kilometrosRecorridos;
	}
	
	public int getCantidadCombustible() {
		return cantidadCombustible;
	}
	
	// Metodos
	public void andar() {
		if (encendido && cantidadCombustible >= 2) {
			kilometrosRecorridos += 10;
			cantidadCombustible -= 2;
		}
		if (cantidadCombustible == 0) {
			encendido = false;
		}
	}
	
	public void encender() {
		if (cantidadCombustible > 0)
			encendido = true;
	}
	
	// Sobreescribir un metodo heredado
	public String toString() {
		String res = "";
		res += "Patente: " + patente;
		res += "\tKM Recorridos: " + kilometrosRecorridos;
		
		return res;
	}
	
	/*
	 * Tipos primitivos:
	 * 		short byte char int long
	 * 		float double
	 * 		boolean
	 */
}
