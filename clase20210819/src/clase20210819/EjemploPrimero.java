package clase20210819;

/*
 * Primera clase ejemplo
 * Para ver como se crean
 */
public class EjemploPrimero {

	public static void main(String[] args) {
		System.out.println("Hola mundo!!!!!");			// print("Hola Mundo")
	
		// Reservar memoria para almacenar
		// Construir el objeto --> invocar a un metodo "especial"
		//						   en la clase
		// Referencia del objeto en memoria
		
		Auto a = new Auto("xxx111");
		//a.patente = "xxx655";
		
		Auto b = new Auto("yyy222");
		//b.patente = "yyyy7777";
		
		// syso y control+espacio
		System.out.println("Patente: " + a.getPatente());
		System.out.println("KM: " + a.getKilometrosRecorridos());
		
		a.encender();
		a.andar();
		
		System.out.println("KM: " + a.getKilometrosRecorridos());
		System.out.println("KM: " + b.getKilometrosRecorridos());
		
		
//		Auto c = new Auto();
//		System.out.println(c.patente);
//		System.out.println(c.kilometrosRecorridos);
//		System.out.println(c.encendido);
		
		System.out.println(a.toString());
		System.out.println(a);
		
		
	}
	
}
