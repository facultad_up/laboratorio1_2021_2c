package clase20211021.vectores;

public class EjemploVectores {
	
	public static void main(String[] args) {
		int[] numeros = new int[5];
		
		for (int i=0; i<numeros.length; i++) {
			System.out.println(numeros[i]);
		}
		System.out.println();
		
		numeros[2] = 22;
		
		System.out.println(numeros[2]);
		
	}
}
