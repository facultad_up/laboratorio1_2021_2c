package clase20211021.listas;

import java.util.ArrayList;

class Persona {
	private int edad;
	private String nombre, apellido;
	
	public Persona(int edad, String nombre, String apellido) {
		this.edad = edad;
		this.nombre = nombre;
		this.apellido = apellido;
	}

	public String toString() {
		return "Persona [edad=" + edad + ", nombre=" + nombre + ", apellido=" + apellido + "]";
	}
}


public class Ejemplo1 {

	public static void main(String[] args) {
		ArrayList lista = new ArrayList();
		
		lista.add("Estoy en la lista");
		lista.add("tambien");
		lista.add(3.14);
		lista.add(new Persona(30, "pepe", "tito"));
		
		System.out.println(lista);
		
		ArrayList<Integer> numeros = new ArrayList<Integer>();
		numeros.add(123);
		numeros.add(506);
		// numeros.add("Puedo?");   No se puede
		
		for (int x : numeros) {
			System.out.println(x);			
		}
		
		int[] mas = new int[5];
		for (int x : mas) {
			System.out.println(x);
		}
	}

}
