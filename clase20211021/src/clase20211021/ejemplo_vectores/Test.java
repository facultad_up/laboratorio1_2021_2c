package clase20211021.ejemplo_vectores;


import java.util.Random;

public class Test {

	public static void main(String[] args) {
//		Camion c1 = new Camion(1000);
//		c1.setCargaActual(2000);
//		
//		Pasajero a = new Pasajero(72);
//		
//		c1.subirPasajero(a);
//		
//		float consumoCamion = c1.recorrer(200);
//		System.out.println(consumoCamion);
		
		Random rnd = new Random();
		Colectivo colec = new Colectivo();
		
		int cantidadPasajeros = rnd.nextInt(25 + 1);
		for (int i=0; i < cantidadPasajeros; i++) {
			float peso = rnd.nextFloat() * 150 + 3;				// nextFloat entre 0 y 1
			
			Pasajero p = new Pasajero(peso);
			colec.subirPasajero(p);
		}
		
		float consumo = colec.recorrer(50.5f);
		System.out.println("Pasajeros: " + cantidadPasajeros);
		System.out.println(consumo);
			
		
		
		
//		Camion c1 = new Camion(1000);
//		c1.setCargaActual(rnd.nextFloat() * 1000);
//		
//		Pasajero a = new Pasajero(rnd.nextFloat() * 150 + 50);		// El peso del pasajero este entre 50 y 200
//		c1.subirPasajero(a);
//		
//		float kilometros = rnd.nextFloat() * 1000 + 100;
//		float consumo = c1.recorrer(kilometros);
//		System.out.println(kilometros);
//		System.out.println(consumo);
		
	}

}
