package clase20211021.ejemplo_vectores;


public class Pasajero {
	private float peso;

	public Pasajero(float peso) {
		this.peso = peso;
	}

	public float getPeso() {
		return peso;
	}
	
}
