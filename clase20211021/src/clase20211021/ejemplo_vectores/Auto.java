package clase20211021.ejemplo_vectores;


public class Auto extends Vehiculo {
	
	public Auto() {
		super(5);	//Invocar al constructor de Vehiculo
	}

	public float recorrer(float kilometros) {
		float consumo = 0.1f; // Arranque
		consumo += (kilometros * this.pesoPasajeros() * 0.002);
		return consumo;
	}
}
