package clase20211021.genericas;

public class Auto<T extends Motor> {
	private T motor;
	
	public void ensamblarMotor(T motor) {
		this.motor = motor;
	}
	
	public static void main(String[] args) {
		// Auto que solo use MotorV2
		Auto<MotorV2> a1 = new Auto<MotorV2>();
		a1.ensamblarMotor(new MotorV2());
		
		
		// Auto que solo use MotorV1
		Auto<MotorV1> a2 = new Auto<MotorV1>();
		
		
		// Auto<Double> a3;   Ya no se puede pq debe ser algo q herede de Motor
		
	}
}
