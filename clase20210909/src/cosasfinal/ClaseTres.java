package cosasfinal;

public class ClaseTres extends ClaseDos {
	// 3. Una vez q toma valor, no se puede modificar
	public final int unaVariable;
	
	public ClaseTres(int valor) {
		this.unaVariable = valor;
	}
	
//	public void metodo1() {
//		System.out.println("Soy Tres");
//	}
	
	public static void main(String[] args) {
		ClaseTres ct = new ClaseTres(50);
		ClaseTres ct2 = new ClaseTres(12);
		
		// ct.unaVariable = 10;
		System.out.println(ct.unaVariable);
		
		// ct.unaVariable = 25;
		System.out.println(ct2.unaVariable);
	}
}
