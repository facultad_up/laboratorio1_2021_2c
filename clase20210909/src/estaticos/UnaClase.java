package estaticos;

public class UnaClase {
	public final static int CONSTANTE1 = 10;
	
	private static int var2;
	
	private int var1;
	
	public void metodo1() {
		System.out.println("Metodo 1....");
	}
	
	public static void metodo2() {
		System.out.println("Metodo 2....");
		var2 = 10;
	}
	
	public static void main(String[] args) {
		UnaClase u = new UnaClase();
		u.metodo1();
		
		
		UnaClase.metodo2();	// No se crea un objeto
		System.out.println(UnaClase.CONSTANTE1);
	}

}
