package interfaces;

public abstract class Animal {
	private float peso;
	
	public abstract void comer();
	
	public void saludar() {
		System.out.println("Hola!!");
	}
}
