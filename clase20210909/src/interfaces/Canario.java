package interfaces;

public class Canario extends Animal implements Volador {

	public void comer() {
		System.out.println("Como alpiste...");
	}

	public void volar() {
		System.out.println("Canario volando...");
	}

}
