package interfaces;

public class Zoo {
	
	public void alimentar(Animal a) {
		a.comer();							// Metodo Polimorfico (Polimorfismo) 
	}
	
	public void hacerVolar(Volador v) {
		v.volar();
	}
	
	public void hacerNadar(Nadador n) {
		n.nadar();
	}
	
	public static void main(String[] args) {
		Pato p = new Pato();
		Canario c = new Canario();
		
		Zoo z = new Zoo();
		
		z.alimentar(p);
		z.alimentar(c);
		
		z.hacerNadar(p);
		System.out.println(p.getKilometrosRecorridos());
		
		z.hacerVolar(p);
		System.out.println(p.getKilometrosRecorridos());
		
		
		System.out.println("\nCastear");
		
		Animal a = new Pato();
		a.comer();
		
		// Cast o amoldar
		((Pato)a).nadar();
		
		if (a instanceof Pato) {
			System.out.println("En a hay un pato");
		}
		if (a instanceof Nadador) {
			System.out.println("Es un nadador");
		}
	}
}
