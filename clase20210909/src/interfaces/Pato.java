package interfaces;

public class Pato extends Animal implements Volador, Nadador {
	
	private int kilomentrosRecorridos;
	
	public int getKilometrosRecorridos() {
		return this.kilomentrosRecorridos;
	}
	
	public void nadar() {
		System.out.println("Pato nadando..");
		this.kilomentrosRecorridos += 1;
	}

	public void volar() {
		System.out.println("Pato volando..");
		this.kilomentrosRecorridos += 5;
	}

	public void comer() {
		System.out.println("Pato comiendo..");
	}

}
